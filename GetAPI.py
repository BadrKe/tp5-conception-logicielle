import requests


def getInformation(code: str):
    request = "https://world.openfoodfacts.org/api/v2/product/" + code
    x = requests.get(request)
    reponse = x.json()

    return(reponse["product"]["ingredients"])


def isVegan(list_ingredients: list[dict[str, str]]):
    resultat = True
    for ingredient in list_ingredients:
        if ingredient["vegan"] != "yes":
            resultat = False

    return(resultat)


liste = getInformation("04963406")
print(isVegan(liste))
