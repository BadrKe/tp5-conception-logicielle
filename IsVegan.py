from GetAPI import getInformation


def isVegan(code: str):
    list_ingredients = getInformation(code)
    resultat = True
    for ingredient in list_ingredients:
        if ingredient["vegan"] != "yes":
            resultat = False

    return(resultat)
